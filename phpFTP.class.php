<?php

class phpFTP {
    public static $version = '0.1';
    
    private $_conn = FALSE;
    private $_homeDir = '';

    public function connect($host, $user, $pass, $homeDir = '/www') {
        try {
            $this->_conn = ftp_connect($host);

            if ($this->_conn) {
                ftp_login($this->_conn, $user, $pass);

                $this->_homeDir = $homeDir;
                $this->changeDir($this->_homeDir);
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function changeDir($newDir) {
        ftp_chdir($this->_conn, $newDir);
    }

    public function createDir($dirName) {
        ftp_mkdir($this->_conn, $dirName);
    }

    public function sendFile($remoteFile, $localFile) {
        ftp_put($this->_conn, $remoteFile, $localFile, FTP_BINARY);
    }

    public function sendDir($path) {
        if (!is_dir($path))
            return FALSE;
        
        $dir_iterator   = new RecursiveDirectoryIterator($path);
        $iterator       = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);
        
        foreach ($iterator as $file) {
            $remoteFile = str_replace($path, '', $file);
            $remoteFile = (substr($remoteFile, 0, 1) === '/') ? substr($remoteFile, 1) : $remoteFile;
            
            $localFile  = realpath($file);
            
            if ($file->getType() === 'dir') {
                $this->_makeDirectory($remoteFile);
            } else {
                $this->sendFile($remoteFile, $localFile);
            }
        }
    }

    private function _makeDirectory($dir) {
        if ($this->_FTPIsDir($dir) || @ftp_mkdir($this->_conn, $dir))
            return TRUE;
        
        if (!$this->_makeDirectory(dirname($dir)))
            return FALSE;
        
        return ftp_mkdir($this->_conn, $dir);
    }

    private function _FTPIsDir($dir) {
        $oDirectory = ftp_pwd($this->_conn);
        if (@ftp_chdir($this->_conn, $dir)) {
            ftp_chdir($this->_conn, $oDirectory);
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function __destruct() {
        if ($this->_conn)
            ftp_close($this->_conn);
    }

}

