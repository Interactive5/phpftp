<?php
    require_once 'phpFTP.class.php';
    
    define('FTP_HOST', '');
    define('FTP_USER', '');
    define('FTP_PASS', '');
    define('FTP_PATH', '');

    $phpFTP = new phpFTP();
    
    $phpFTP->connect(FTP_HOST, FTP_USER, FTP_PASS, FTP_PATH);
    $phpFTP->sendDir('templates/wordpress-3.4.2');